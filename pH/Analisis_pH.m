Datos = dlmread("pH_test.csv");
pH = Datos(:,1);
R = Datos(:,2)*1.5;
G = Datos(:,3)*1.5;
B = Datos(:,4)*1.5;

figure 1
plot(pH, R, "-r*");
hold on
plot(pH, G, "-g*");
plot(pH, B, "-b*");
hold off

figure 2
plot( pH, R./(R+G+B));