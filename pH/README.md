# Experimento de determinar el pH de una solución usando el colorímetro.

## Espectativas de logro:
- Comprender el concepto de espectro de la luz.
- Comprender la ley de Beer-Lambert.
- Entender el funcionamiento del colorímetro.
- Identificar posibles aplicaciones del colorímetro en el laboratorio de química analítica.

## Materiales
- Colorímetro.
- 10 matraces aforados de 50ml.
- 2 matraces de 500ml.
- Pipeta de 1ml graduada y propipeta.
- Pipeta de 25ml graduada.
- Cubetas plásticas para el colorímetro.
- Agua destilada.
- Hidróxido de sodio.
- Fosfato de potasio.
- Azul de bromotimol.
- 2 vasos de precipitado de 50ml.
- 1 vaso de precipitado de 100ml.

![Figura 1](https://gitlab.com/pcremades/Taller_Arduino/raw/master/pH/Img/Material.jpg)


## Experiencia sugerida.
1. Prepare 500ml de solución de hidróxido de sodio 0.2M.
2. Prepare 500ml de solución de fosfato de potasio 0.2M.
3. Prepare las soluciones en los matraces aforados siguiendo los datos de la tabla, enrazando con agua destilada.

|   pH   |   KH<sub>2</sub>PO<sub>4</sub>   |    NaOH    | 
| ------ | :---------: | :--------: |
|  5.8   |     25ml    |   1.86ml   |
|  6.0   |     25ml    |   2.85ml   |
|  6.2   |     25ml    |   4.30ml   |
|  6.4   |     25ml    |   6.30ml   |
|  6.6   |     25ml    |   8.90ml   |
|  6.8   |     25ml    |   11.83ml  |
|  7.0   |     25ml    |   14.82ml  |
|  7.4   |     25ml    |   19.75ml  |
|  7.8   |     25ml    |   22.60ml  |
|  8.0   |     25ml    |   23.40ml  |

4. Coloque 5 gotas de azul de bromotimol en cada matraz.
![Figura 2](https://gitlab.com/pcremades/Taller_Arduino/raw/master/pH/Img/Matraces.jpg)

5. Coloque una muestra de cada matraz en una cubeta plástica.
![Figura 3](https://gitlab.com/pcremades/Taller_Arduino/raw/master/pH/Img/Cubetas.jpg) 

## Curva de calibración.
Para crear la curva de calibración del colorímetro debe colocar una cubeta llena con agua destilada en el colorímetro.
Presione el botón "Calibrar".
Luego coloque cada una de las muestras en el colorímetro y mida la absorbancia en el canal VERDE registrando los datos en una tabla.

![Figura 4: aplicación de Processing.](https://gitlab.com/pcremades/Taller_Arduino/raw/master/pH/Img/Aplicacion.png)

Finalmente grafique los datos de absorbancia vs. pH.

![Figura 5](https://gitlab.com/pcremades/Taller_Arduino/raw/master/pH/Img/pHvsAbs.png)

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Licencia Creative Commons Atribución-NoComercial 4.0 Internacional</a>.
