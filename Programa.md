# Taller sobre Arduino: aplicaciones en la enseñanza de ciencias básicas.
Docente responsable: Dr. Pablo Cremades

Contacto: pablocremades@gmail.com

## Programa

### Tecnología libre:
Conceptos básicos y la diferencia con tecnología propietaria.
Patentes y licencias.
Breve reseña histórica del software libre.
Tecnología libre en el ámbito científico y académico. Ejemplos.

### Herramientas de prototipado rápido y el movimiento DIY:
Herramientas de control numérico (CNC).
Impresoras 3D.
Placas de desarrollo con microcontroladores (CIA, Arduino).

### Arduino:
Introducción a la plataforma Arduino. IDE de Arduino. Introducción a la programación.
Funciones setup() y loop().
Entradas y salidas digitales. Entradas analógicas. Comunicación con la PC.
	
### Introducción a Processing:
Introducción a programación orientada a objetos. IDE de Processing.
Definición de Clases. Definición de objetos.
Estructura básica de un programa en Processing. Funciones setup() y draw().
Captura de eventos de teclado y mouse.

### Interfaz entre Arduino y Processing
Comunicación por puerto serie. Captura y procesamiento de datos en Processing.
Adquisición, procesamiento y visualización de datos.
Aplicaciones prácticas con Processing: desarrollo de interfaces gráficas sencillas.

### Desarrollo de instrumentos de laboratorio con Arduino
1. Fotocompuerta.
Medición de tiempo con fotocompuerta. Medición de velocidad con fotocompuerta.
Construcción de una fotocompuerta. Conceptos básicos de electrónica: tensión, corriente, ley de ohm. 
LED y fototransistor.
Aplicación: cinemática. Caída libre. Determinación de la aceleración de la gravedad con fotocompuerta.
Aplicación: movimiento armónico. Determinación del período de un péndulo simple con fotocompuerta.
2. Colorímetro.
Principio de funcionamiento. Ley de Beer-Lambert. Construcción del colorímetro.
LED RGB. Sensor de intensidad de luz. Curva de calibración del colorímetro.
Aplicación: determinación de concentración de soluciones en el laboratorio de química.

