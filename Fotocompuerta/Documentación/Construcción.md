# Construcción de la Fotocompuerta

## Materiales
 - Arduino, cualquier variante.
 - LED infrarrojo [ir383](tienda.ityt.com.ar/mercadolibre/2581-ir383-reemp-l53f3bt-led-emisor-infrarrojo-940nm-5mm-itytarg.html?search_query=infrarrojo&results=69) o equivalente
 - Fototransistor infrarrojo [LPT3323](http://tienda.ityt.com.ar/mercadolibre/2561-lpt3323-fototransistor-receptor-940nm-infrarrojo-itytarg.html?search_query=infrarrojo&results=69) o equivalente 
 - Resistencia de 100 ohm (marrón-negro-marrón).
 - Resistencia de 330 kOhm (naranja-naranja-amarillo).
 - Carcaza plástica hechas mediante impresión 3D [link](https://gitlab.com/pcremades/Taller_Arduino/tree/master/Fotocompuerta/Hardware/Carcaza%20sensores).
 
## Armado

### 1. Inserte el LED y el fototransistor en la carcaza como se muestra en la figura 1.
![Figura 1](https://gitlab.com/pcremades/Taller_Arduino/raw/master/Fotocompuerta/Img/fotogate%20abierta.jpg)

### 2. Conecte el LED y el fototransistor siguiendo el esquema de la figura 2.
![Figura 2](https://gitlab.com/pcremades/Taller_Arduino/raw/master/Fotocompuerta/Img/Diagrama.png)

El ánodo del LED y el colector del fototransistor son los que correspoenden a la pata más larga. Éstas se conectan
a los respectivos resistores. Puede ver el esquema eléctrico [aquí](https://gitlab.com/pcremades/Taller_Arduino/raw/master/Fotocompuerta/Hardware/Electronica/Esquematico.pdf) para más detalles.

### 3. Cargue el [firmware](https://gitlab.com/pcremades/Taller_Arduino/raw/master/Fotocompuerta/Software/firmware/firmware.ino) en el Arduino.

## Verificación
Abra el monitor serial en el IDE de Arduino y corrobore que recibe un mensaje cada vez que un objeto interrumpe la fotocompuerta.

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Licencia Creative Commons Atribución-NoComercial 4.0 Internacional</a>.
