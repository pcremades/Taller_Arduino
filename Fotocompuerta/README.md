# Fotocompuerta LabFD
![Fotocompuerta](https://gitlab.com/pcremades/Taller_Arduino/raw/master/Fotocompuerta/Img/fotogate%20cerrada.jpg)

Esta es una fotocompuerta simple y de bajo costo para experimentos de física.

Está basada en un [Led emisor](http://tienda.ityt.com.ar/mercadolibre/2581-ir383-reemp-l53f3bt-led-emisor-infrarrojo-940nm-5mm-itytarg.html?search_query=infrarrojo&results=69) y un [receptor IR](http://tienda.ityt.com.ar/mercadolibre/2561-lpt3323-fototransistor-receptor-940nm-infrarrojo-itytarg.html?search_query=infrarrojo&results=69)

Puede encontrar toda la información para el armado paso a paso [aquí](https://gitlab.com/pcremades/Taller_Arduino/blob/master/Fotocompuerta/Documentaci%C3%B3n/Construcci%C3%B3n.md)

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Licencia Creative Commons Atribución-NoComercial 4.0 Internacional</a>.
