/* Copyright 2015 Pablo Cremades //<>//
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**************************************************************************************
 * Autor: Pablo Cremades
 * Fecha: 30/07/2018
 * e-mail: pablocremades@gmail.com
 * Descripción: Esta aplicación sirve para verificar el funcionamiento de la fotocompuerta.
 *  Simplemente dibuja en la pantalla un círculo que cambia de color según el estado de la fotocompuerta.
 *  Si no hay ningún obstáculo el círculo es verde. Cuando se interrumpe el haz, cambia a rojo.
 *
 * Change Log:
 */


import processing.serial.*;


String[] serialPorts;
Serial port;

String inString;

void setup() {
  size(800, 480);

  if ( openComm() == 0 ) {
    //Iniciar placa INGKA
    startINGKA();
    setPGate();
  }
  
  ellipseMode(CENTER);
}

String[] list;
int status;

void draw() {
  background(230);
  stroke(0);
  strokeWeight(3);
  if( status == 0 )
    fill(0, 255, 0);
  else if( status == 1 )
    fill(255, 0, 0);
  ellipse(width/2, height/2, 100, 100);
}

void serialEvent(Serial port) { 
  inString = port.readString();
  //Buscamos un string válido y no leemos más de 16 strings.
  if ( inString.length() > 10 && inString.charAt(0) != '#') {
    list = split(inString, "\t"); //Split the string.
    status = Integer.parseInt(list[2].trim());  //Status is in the 3rd substring //<>//
  }
  inString ="";
}

int openComm() {
  serialPorts = Serial.list(); //Get the list of tty interfaces
  for ( int i=0; i<serialPorts.length; i++) { //Search for ttyACM*
    if ( serialPorts[i].contains("ttyACM") || serialPorts[i].contains("ttyUSB0") || serialPorts[i].contains("COM") ) {  //If found, try to open port.
      println(serialPorts[i]);
      try {
        port = new Serial(this, serialPorts[i], 115200);
        port.bufferUntil(10);
      }
      catch(Exception e) {
        return 1;
      }
    }
  }
  if (port != null)
    return 0;
  else
    return 1;
}


//Inicia el SAD de INGKA enviando los códigos de autenticación.
void startINGKA() {
  port.write("#0001");  //Inicio.
  delay(100);
  port.write("#0031 462815232 1943863296 831782912 1421148160 ");  //Código de autenticación
  delay(100);
  port.write("#0033 239589820 3486795892 3188765557 2136465651 ");  //Código de autenticación
  delay(100);
}

//Configura la fotocompuerta conectada a la entrada digital 1 del SAD.
void setPGate() {
  port.write("#0007");
  delay(100);
  port.write("#0005,2,3,2,3,10,1,0,1,1,1,1,1,1,1");  //Sensor 0
  delay(100);
  //port.write("#0005,3,3,2,3,15,1,0,1,1,1,1,1,1,1");  //Sensor 1
  delay(100);
}