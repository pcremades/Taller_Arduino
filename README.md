# Taller sobre Arduino: aplicaciones en la enseñanza de ciencias básicas.
Docente responsable: Dr. Pablo Cremades
Contacto: pablocremades@gmail.com

### Descripción
Este taller está orientado a docentes de nivel medio y universitario, estudiantes y público en general interesado en desarrollo de **tecnología libre** para enseñanza de ciencias básicas.
El taller propone explorar aspectos generales sobre herramientas de prototipado rápido (fabricación mediante impresión 3D), **electrónica básica** como herramienta para desarrollar instrumentos de laboratorio, y la plataforma **Arduino**. Por otro lado, se introduce el lenguaje de programación Processing, su aplicación al desarrollo de interfaces con el usuario y la interacción con Arduino.

### Objetivos y Expectativas de Logro
- Comprender la importancia de la tecnología libre como herramienta para dar respuesta a problemas sociales en general.

- Aprender conceptos generales sobre la plataforma de desarrollo Arduino y sus aplicaciones a soluciones concretas.

- Introducir el lenguaje de programación Processing.

- Desarrollar instrumentos para uso en laboratorio de ciencias básicas utilizando la plataforma Arduino.

### Requisitos
Este curso no requiere conocimientos previos de electrónica ni de programación. Sí es recomendable conocimiento intermedio de informática (instalación de aplicaciones, manejo de archivos y directorios, configuración básica de la PC). También es recomendable un conocimiento medio de inglés puesto que gran parte de la documentación está en Inglés.
Es también recomendable tener sólidos conocimientos de matemática y física de nivel medio.
Es muy recomendable tener computadora propia para poder realizar las prácticas.

### Metodología
El curso tendrá formato taller que constará de 8 encuentros de 4 horas cada uno. Se desarrollarán dos intrumentos de laboratorio basados en Arduino y los contenidos cubrirán desde las aplicaciones en el aula hasta el armado y funcionamiento de los mismos.
Cupo limitado: 20 personas.

[Programa del curso](https://gitlab.com/pcremades/Taller_Arduino/blob/master/Programa.md)

### Evaluación
El curso se aprobará con la presentación escrita y oral de una propuesta didáctica de laboratorio utilizando tecnología libre. Para acceder a la instancia de exámen, se debe acreditar una asistencia mínima del 80%.

### Acerca de Pablo Cremades
Estudié Ingeniería en Electrónica y Telecomuniciones en la Universidad de Mendoza (2009) y realicé mi doctorado en Ingeniería en la Universidad Nacional de Cuyo, trabajando en temas relacionados con calidad de aire y contaminación ambiental. Desde 2015 soy docente de la Facultad de Ciencias Exactas y Naturales de la Universidad Nacional de Cuyo (http://fcen.uncuyo.edu.ar/fisica-interactiva/) donde estoy a cargo de los laboratorios de física. Desde entonces trabajo en el desarrollo de tecnología libre para la enseñanza de ciencias básicas. [CV completo](https://gitlab.com/pcremades/Taller_Arduino/blob/master/CVar_final.pdf)

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Licencia Creative Commons Atribución-NoComercial 4.0 Internacional</a>.


