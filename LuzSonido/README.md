# Experimento simple de medición de luz.

## Descripción
Utilizamos un fototransistor conectado a Arduino para medir la intensidad de la luz ambiente.
Esa información se envía a la computadora y generamos un tono cuya frecuencia cambia con
la intensidad de la luz.

## Materiales
- Fototransistor [LPT3323](http://tienda.ityt.com.ar/mercadolibre/2561-lpt3323-fototransistor-receptor-940nm-infrarrojo-itytarg.html?search_query=infrarrojo&results=69).
- Resistencia de 100kOhm.
- Arduino.
- Computadora con Processing instalado.
- [Firmaware Arduino](https://gitlab.com/pcremades/Taller_Arduino/raw/master/LuzSonido/firmware.ino)
- [Aplicación de Processing](https://gitlab.com/pcremades/Taller_Arduino/raw/master/LuzSonido/LuzSonido.pde)

## Conexión
![figura 1](https://gitlab.com/pcremades/Taller_Arduino/raw/master/LuzSonido/Img/esquematico.png)


