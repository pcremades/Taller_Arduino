/* Copyright 2015 Pablo Cremades //<>// //<>//
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**************************************************************************************
 * Autor: Pablo Cremades
 * Fecha: 14/09/2018
 * e-mail: pablocremades@gmail.com
 * Descripción: 
 * Change Log:
 */


import processing.serial.*;
import processing.sound.*;


String[] serialPorts;
Serial port;

String inString;
float luz=0;
SinOsc sine;

void setup() {
  size(800, 480);

  openComm();
  
  sine = new SinOsc(this);
  sine.play();
  sine.amp(1.0);
}

String[] list;

void draw() {

  background( 200 );
  float freq = map( luz, 0, 1023, 200.0, 3000.0);
  sine.freq(freq);
}

//Lee los datos del puerto cada vez que llega algo. Deja de leer luego de recibir un
//cierto número de strings ya que no tiene sentido leer datos indefinidamente.
void serialEvent(Serial port) { 
  inString = port.readString();
  //Buscamos un string válido y no leemos más de 16 strings.
  if ( inString.length() > 3 ) {
    //print(inString);
    list = split(inString, "\t"); //Split the string.
    luz =  Float.parseFloat(list[0]);
  }
  inString ="";
}

int openComm() {
  serialPorts = Serial.list(); //Get the list of tty interfaces
  for ( int i=0; i<serialPorts.length; i++) { //Search for ttyACM*
    if ( serialPorts[i].contains("ttyACM") || serialPorts[i].contains("ttyUSB0") || serialPorts[i].contains("COM") ) {  //If found, try to open port.
      println(serialPorts[i]);
      try {
        port = new Serial(this, serialPorts[i], 115200);
        port.bufferUntil(10);
      }
      catch(Exception e) {
        return 1;
      }
    }
  }
  if (port != null)
    return 0;
  else
    return 1;
}