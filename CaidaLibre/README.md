# Experimento de caída libre para determinar la aceleración de la gravedad.

## Espectativas de logro:
- Comprender la diferencia entre velocidad instantánea y velocidad media.
- Comprender la diferencia entre aceleración instantánea y aceleración media.
- Entender el funcionamiento de la fotocompuerta.
- Identificar posibles aplicaciones de la fotocompuerta en el laboratorio de física.

## Materiales
- [Fotocompuerta](https://gitlab.com/pcremades/Taller_Arduino/tree/master/Fotocompuerta).
- Regla transparente con franjas oscuras igualmente espaciadas.
- Computadora con [Processing](https://processing.org/) instalado.
![figura 1] ------------------------------------------------------------Fotos!!!!

## Preparando la experiencia
Descargue la [aplicación](https://gitlab.com/pcremades/Taller_Arduino/blob/master/CaidaLibre/CaidaLibre.pde) de Processing. Abra el archivo Caidalibre.pde en Processing.
Conecte el Arduino con la fotocompuerta a la computadora. Ejecute la aplicación en Processing.

## Descripción de la aplicación.
La regla se deja caer a través de la fotocompuerta. El sistema registra la posición y el tiempo a cada interrupción. Los puntos Posción/Tiempo se grafican instantáneamente.
La aplicación también muestra la tabla de datos registrados.
El usuario debe cargar el dato de la distancia entre franjas (en cm) que tiene la regla. IMPORTANTE: Se asume que las franjas transparentes y oscuras tienen el mismo ancho.
Con el botón "Reiniciar" se borran los datos para realizar una nueva experiencia.
El botón "Ajuste" dibuja una parábola definida por 3 puntos (azules) que pueden moverse haciendo click con el mouse y arrástrándolos. Esto permite hacer una ajuste manual
de los datos. La aplicación muestra la ecuación horaria ( y(t) = a.t^2 + b.t +c ) correspondiente a dicha parábola. Cuando se ha ajustado la parábola
a los datos experimentales, el coeficiente "a" en la ecuación corresponde a g/2 (la aceleración de la gravedad).

![Fifura 2: aplicación de Processing.](https://gitlab.com/pcremades/Taller_Arduino/raw/master/Img/ScreenShotCaidaLibre_1.png)

## Experiencia sugerida.
1. Construya 3 zebras con distinto espaciamiento y cantidad de franjas.
2. Realice el experimento tirando la zebra de franjas más anchas por la fotocompuerta para registrar los datos.
3. Calcule la velocidad media tomando puntos consecutivos.
4. Con los datos anteriores, calcule la aceleración media en cada tramo.
5. Repita para las otras 2 zebras, mostrando cómo el valor de la aceleración mejora al disminuir el ancho de las franjas.
6. Introduzca el concepto de límite: velocidad y aceleración instantánea.
