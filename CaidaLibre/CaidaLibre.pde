/* Copyright 2015 Pablo Cremades //<>//
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**************************************************************************************
 * Autor: Pablo Cremades
 * Fecha: 30/07/2018
 * e-mail: pablocremades@gmail.com
 * Descripción: Esta aplicación sirve para hacer experimentos de caída libre. Se utiliza una fotocompuerta y una regla transparente
 *  con franjas negras (o cuanquier material/ pintura que bloque la luz) igualmente espaciadas. La regla se deja caer a través de
 *  la fotocompuerta. El sistema registra la posición y el tiempo a cada interrupción. Los puntos se grafican instantáneamente.
 *  La aplicación también muestra la tabla de datos registrados.
 *  El usuario debe cargar el dato de la distancia entre franjas que tiene la regla. Se asume que las franjas transparentes y
 *  oscuras tienen el mismo ancho.
 *  Con el botón "Reiniciar" se borran los datos para realizar una nueva experiencia.
 *  El botón "Ajuste" dibuja una parábola definida por 3 puntos (azules) que pueden moverse. Esto permite hacer una ajuste manual
 *  de los datos. La aplicación muestra la ecuación horaria ( y(t) = a.t^2 + b.t +c ) correspondiente a dicha parábola. Cuando se ha 
 *  ajustado la parábola a los datos experimentales, el coeficiente "a" en la ecuación corresponde a g/2 (la aceleración de la gravedad).
 *
 * Change Log:
 */


import grafica.*;
import processing.serial.*;
import g4p_controls.*;


String[] serialPorts;
Serial port;

public GPlot plotPos, plotVel;

GButton Reiniciar;
GButton Ajuste;
boolean ajuste = false;
GTextField gridSpace;

String inString;

float gridSpacing = 1.8;  //Espaciado entre las franjas de la regla. Valor por defecto.

//Parábola de ajuste. Se definen 2 arreglos de puntos. Uno con 3 puntos, que definen a la parábola y que se puede mover.
//El otro arreglo (parabola) tiene ParabolaN puntos definidos a partir de los otros 3 y se usa para graficar con lína
//de trazo fino la parábola propiamente dicha. Los valores a,b y c son los coeficientes de la ec. horaria.
GPointsArray parabolaPoints = new GPointsArray( new float[] {0, 60, 80}, new float[] {0.1, 0.2, 0.3});  //Coordenadas iniciales de los puntos que definen la parábola de ajuste.
int ParabolaN = 10;
GPointsArray parabola;
float a, b, c; //Coeficientes de la parábola.

void setup() {
  size(800, 480);

  if ( openComm() == 0 ) {
    //Iniciar placa INGKA
    startINGKA();
    setPGate();
  }

  //Definimos los controles.
  Reiniciar = new GButton( this, width - 120, height - 50, 100, 20, "Reiniciar" );
  Ajuste = new GButton(this, 20, height - 50, 100, 20, "Ajuste" );
  gridSpace = new GTextField(this, width - 300, height - 50, 100, 20);
  gridSpace.setText(str(gridSpacing));

  //Definimos el gráfico. Son 3 layes: Main, Ajuste y Parabola. El layer Ajuste tiene los 3 puntos que se pueden
  //mover y que defienen la parábola. El layer Parabola tiene la línea de trazo fino. El layer Main tiene los datos experimentales.
  plotPos = new GPlot(this);
  plotPos.setPos(20, 20);
  plotPos.setDim(420, 260);
  plotPos.setXLim(0, 300);
  plotPos.getTitle().setText("Posición vs Tiempo");
  plotPos.getXAxis().getAxisLabel().setText("Tiempo [ms]");
  plotPos.getYAxis().getAxisLabel().setText("Posición [cm]");
  plotPos.addLayer("Ajuste", new GPointsArray()); 
  plotPos.getLayer("Ajuste").addPoints(parabolaPoints);
  plotPos.getLayer("Ajuste").setXLim(plotPos.getXLim());
  plotPos.getLayer("Ajuste").setYLim(plotPos.getYLim());
  //plotPos.getLayer("Ajuste").setDim( plotPos.getDim() );
  plotPos.getLayer("Ajuste").setPointColors(new int[] {color(0, 0, 255)});
  parabola = new GPointsArray( new float[ParabolaN+1], new float[ParabolaN+1]);
  plotPos.addLayer("Parabola", parabola);
  plotPos.getLayer("Parabola").setXLim(plotPos.getXLim());
  plotPos.getLayer("Parabola").setYLim(plotPos.getYLim());

  println("Empezamos");
}

int oldStatus, status;
float tiempo, initialTime;
boolean firstIntercept = true;
String[] list;
int pos, oldPos; //Posición en cm;

void draw() {
  gridSpacing = float( gridSpace.getText() );  //Actualiza el valor de gridSpacing
  if ( Float.isNaN(gridSpacing) )  //Si se introduce un caracter no válido, la línea anterior devuelve NaN. Verificamos.
    gridSpacing = 1;

  background( 200 );
  fill(0);
  textSize(16);
  text("Posición[cm]   Tiempo[ms]", width-240, 40);
  text("Espacio de grilla: ", width - 440, height - 35);

  // Dibuja posPlot
  plotPos.beginDraw();
  plotPos.drawBackground();
  plotPos.drawBox();
  plotPos.drawXAxis();
  plotPos.drawYAxis();
  plotPos.drawTitle();
  plotPos.drawGridLines(GPlot.BOTH);
  plotPos.getMainLayer().drawPoints();
  //Si se ha seleccionado "Ajuste" se dibuja la parábola y se calcula en tiempo real los parámetros de la ec. horaria.
  if ( ajuste == true ) {
    float x1, x2, x3=0, y1, y2, y3=0;  //Unos cambios de nombre de variable para simplificar el código.
    plotPos.getLayer("Ajuste").drawPoints();  //Dibuja los 3 puntos que definen la parábola.
    x1 = parabolaPoints.getX(0); 
    y1 = parabolaPoints.getY(0);
    x2 = parabolaPoints.getX(1); 
    y2 = parabolaPoints.getY(1);
    x3 = parabolaPoints.getX(2); 
    y3 = parabolaPoints.getY(2);
    //Calculamos los coeficientes a,b,c de la parábola que acabamos de determinar.
    float A1 = -x1*x1 + x2*x2; 
    float A2 = -x2*x2 + x3*x3;
    float B1 = -x1 + x2; 
    float B2 = -x2 + x3;
    float D1 = -y1 + y2; 
    float D2 = -y2 + y3;
    float Bmult = -B2/B1;
    float A3 = Bmult*A1 + A2;
    float D3 = Bmult*D1 + D2;
    a = D3/A3;
    b = (D1 - A1*a)/B1;
    c = y1 - a*x1*x1 - b*x1;
    //Calculamos los ParabolaN puntos para dibujar la parábola con trazo fino.
    for ( int i = 0; i < ParabolaN+1; i++) {
      plotPos.getLayer("Parabola").addPoints(new GPointsArray());
      float x = lerp( parabolaPoints.getX(0), parabolaPoints.getX(2), float(i)/ParabolaN);
      //Esta es la ec. de una parábola que pasa por 3 pusntos (x,y).
      float y = (x - x2)*(x-x3)/(x1-x2)/(x1-x3)*y1 + (x - x1)*(x-x3)/(x2-x1)/(x2-x3)*y2 + (x - x1)*(x-x2)/(x3-x1)/(x3-x2)*y3;   
      parabola.setX(i, x);
      parabola.setY(i, y);
    }
    //Escribimos la ec. horaria. Hay unos factores para pasar de cm/ms^2 a m/s^2
    plotPos.drawAnnotation("y(t) = "+float(round(a*10000*100))/100+"*t^2 + "+round(b*1000*100)/100+"*t", x3, y3, LEFT, LEFT);
    plotPos.getLayer("Parabola").setPoints(parabola);
    plotPos.getLayer("Parabola").drawLines();
  }
  plotPos.endDraw();

//Dibujamos el indicador de Ajuste ON/OFF
  if ( ajuste )
    fill(0, 250, 0);
  else
    fill(250, 0, 0);
  stroke(0);
  ellipse(150, height - 40, 20, 20);

//Esbribimos la tabla de datos.
  textSize( 14 );
  fill(0);
  for ( int i = 0; i< plotPos.getPoints().getNPoints(); i++ ) {
    text( str(round(plotPos.getPoints().getY(i)*100)/100.0), 600, 60 + i*20);
    text( str(round(plotPos.getPoints().getX(i))), 700, 60 + i*20);
  }
}

//Lee los datos del puerto cada vez que llega algo. Deja de leer luego de recibir un
//cierto número de strings ya que no tiene sentido leer datos indefinidamente.
void serialEvent(Serial port) { 
  inString = port.readString();
  //Buscamos un string válido y no leemos más de 16 strings.
  if ( inString.length() > 10 && inString.charAt(0) != '#' && pos < 16 ) {
    //print(inString);
    list = split(inString, "\t"); //Split the string.
    println(list[1]); //<>//
//    int sensor = Integer.parseInt(list[0]) - 2; //Los sensores digitales son 2 y 3. Restamos 2 para usar de index (0 y 1).
    status = Integer.parseInt(list[2].trim());  //Status is in the 3rd substring
    if ( status != oldStatus ) {  //If status for any sensor changed from last time...
      tiempo =  Float.parseFloat(list[1]);
      if ( firstIntercept == true ) {  //Primera franja detectada.
        firstIntercept = false;
        initialTime = tiempo;
        plotPos.setPoints(new GPointsArray());
        plotPos.addPoint(0, 0);
      } else {
        pos++; 
        plotPos.addPoint( (tiempo - initialTime)/1000, pos*gridSpacing );
        println( (tiempo-initialTime)/1000, pos*gridSpacing);
        //text( pos, width-200, 40);
      }
      oldStatus = status; //Update sensor status.
    }
  }
  inString ="";
}

int openComm() {
  serialPorts = Serial.list(); //Get the list of tty interfaces
  for ( int i=0; i<serialPorts.length; i++) { //Search for ttyACM*
    if ( serialPorts[i].contains("ttyACM") || serialPorts[i].contains("ttyUSB0") || serialPorts[i].contains("COM") ) {  //If found, try to open port.
      println(serialPorts[i]);
      try {
        port = new Serial(this, serialPorts[i], 115200);
        port.bufferUntil(10);
      }
      catch(Exception e) {
        return 1;
      }
    }
  }
  if (port != null)
    return 0;
  else
    return 1;
}


//Inicia el SAD de INGKA enviando los códigos de autenticación.
void startINGKA() {
  port.write("#0001");  //Inicio.
  delay(100);
  port.write("#0031 462815232 1943863296 831782912 1421148160 ");  //Código de autenticación
  delay(100);
  port.write("#0033 239589820 3486795892 3188765557 2136465651 ");  //Código de autenticación
  delay(100);
}

//Configura la fotocompuerta conectada a la entrada digital 1 del SAD.
void setPGate() {
  port.write("#0007");
  delay(100);
  port.write("#0005,2,3,2,3,10,1,0,1,1,1,1,1,1,1");  //Sensor 0
  delay(100);
  //port.write("#0005,3,3,2,3,15,1,0,1,1,1,1,1,1,1");  //Sensor 1
  delay(100);
}

//Manejador de eventos de los Botones "Reiniciar" y "Ajuste".
public void handleButtonEvents(GButton button, GEvent event) {
  if ( button == Reiniciar ) {
    firstIntercept = true;
    pos = oldPos = 0;
    plotPos.setPoints( new GPointsArray() );
  }
  if ( button == Ajuste ) {
    ajuste = !ajuste;
  }
}


//Manejador de evento mouseDragged. Mueve los puntos que definen la parábola cuando se mantiente
//presionado el botón del mouse sobre uno de ellos.
int pointIndx;
float[] plotpos;
void mouseDragged() {
  if ( plotPos.isOverPlot(mouseX, mouseY)) {  //Detecta si el mouse está dentro del gráfico.
    plotpos = plotPos.getPlotPosAt(mouseX, mouseY);  //Convierte las coordenadas del mouse al sistema de referencia del gráfico.
    //Vemos si hay algún punto de la parábola cerca del mouse. Si es así, el método nos devuelve el índice dentro del GPointsArray.
    //Si no, devuelve -1.
    pointIndx = plotPos.getLayer("Ajuste").getPointIndexAtPlotPos(plotpos[0], plotpos[1]);
    if ( pointIndx != -1 ) {
      parabolaPoints.remove(pointIndx);  //Si encontramos un punto cerca, lo quitamos del vector y lo agregamos nuevamente en las nuevas coordenadas.
      parabolaPoints.add(pointIndx, plotPos.getValueAt(mouseX, mouseY)[0], plotPos.getValueAt(mouseX, mouseY)[1]);
      plotPos.getLayer("Ajuste").setPoints(parabolaPoints);  //Actualizamos los puntos en el gráfico.
    }
  }
}