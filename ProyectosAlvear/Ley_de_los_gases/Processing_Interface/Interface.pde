/*
Copyright 2018 Facundo Lattandi
Autor: Facundo Lattandi
e-mail: facundolattandi@gmail.com
This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import processing.serial.*;
Serial port;
float y=400;
float temp=0;
float k,f;
PImage termometro;
PImage flecha;
String val="";
color boton;
void setup() {
  size(600, 500);
  //Si no encontro ningun arduino
  if ( openComm() == 1 ) {
    println( "No hay ningun Arduino conectado" );
    exit();
  }
  termometro = loadImage("thermometer-305319_960_720.png");
  flecha = loadImage("flecha.png");
  background(255,146,3);
}
void draw() {
  background(definirColor());
  //coloca el termometro
  image(termometro, 15, 125, 150, 300);
  //Crea cuadrados para tapar variaciones
  fill(0, 10, 100);
  rect(0, 0, 600, 100);
  fill(definirColor());
  noStroke();
  rect(140, 107, 100, 350);
  
  
  //fill(255,146,3);
  fill(3,255,102);
  rect(230, 115, 350,100);
  
  //Si ya encontro el arduino osea que ya cambio la variable de "" a EJ"25"
  if (val!="") {
    fill(250, 255, 3);
    textSize(40);
    text("La temperatura es: "+val, 10, 60);
    text("ºC", 500, 60);
    temp=Float.parseFloat(val);
    y=map(temp,0,50,400,100);
    image(flecha, 140, y, 65, 50);
    
    //copy(113,(int)y,1,1,10,433,50,50);Se podria utlizar este metodo que toma el color del termometro pere es ineficiente no se nota tanta diferencia
    fill(boton);
    rect(10,433,50,50);
    
    //Pone la temperatura en ºF y ºK
    fill(0,10,250);
    textSize(30);
    //ºF
    
    f=((temp*1.8)+32);
    float f2 = f - f%0.01;
    text("Fahrenheit: "+f2, 240, 150); strokeWeight(10);fill(3,255,102);  rect(500,115,80,50); noStroke();//Por si sobrepasa los 2 desimales crea un cuadrado 
    fill(0,10,250);
    text("ºF", 500, 150);
    //ºK
    k=temp+273.15;
    text("Kelvin: "+k, 240, 200);
    text("ºK", 449, 200);
    
    //Colocamos el de fondo
    boton = definirColor();
    
  }
  //Si todavia no encuentra el sensor 
  if (val=="") {
    textSize(40);
    fill(250, 255, 3);
    text("CONECTANDO...", 10, 60);
    
  }
  color fondo;
  fondo=definirColor();
  println("MouseX:"+mouseX+" // MouseY:"+mouseY+" // Flecha y:"+y+" //  Y color de fondo: "+fondo);
}
//Interrupcion de temperatura
void serialEvent(Serial puerto) {
  val =puerto.readString();
}


//Busca el puerto Al queconectamos el arduino
int openComm() {
  String[] serialPorts = Serial.list(); //Get the list of tty interfaces
  for ( int i=0; i<serialPorts.length; i++) { //Search for ttyACM*
    if ( serialPorts[i].contains("ttyACM") || serialPorts[i].contains("ttyUSB0") || serialPorts[i].contains("COM") ) {  //If found, try to open port.
      println(serialPorts[i]);
      try {
        port = new Serial(this, serialPorts[i], 9600);
        port.bufferUntil(10);
      }
      catch(Exception e) {
        return 1;
      }
    }
  }
  if (port != null)
    return 0;
  else
    return 1;
}
color definirColor(){
  int temp2=(int)map(temp,0,50,0,255);
  color Color;
  int R=0;
  int G=0;
  int B=0;
  
  R=250;
  B=(int)map(temp2,0,100,250,0);
  R= (R-B);
  G=(int)((temp2*-1)+300);
  if(temp2<=20){
   R-=200;
   G-=200;
   B=(int)((temp2*-1)+300);
  }
  if(temp2<=51){
  B+=100;
  }
  if(temp2>=51&&temp2<=102){
   B-=50;
   R-=200;
   G=(int)((temp2*-1)+300);
  }
  
  Color=color(R,G,B);
  return Color;
}
