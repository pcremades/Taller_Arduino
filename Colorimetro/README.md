﻿# Colorímetro

![Colorímetro](https://gitlab.com/pcremades/Taller_Arduino/raw/master/Colorimetro/Img/Colorimetro.jpg)
Adaptación del colorímetro de IO Rodeo [https://bitbucket.org/iorodeo/colorimeter/src/default/]

Apuntes del colorímetro [https://docs.google.com/document/d/1vM2MXbsRmXswq5W6U_234DTH7rzHVtUj7k6jOHHOmn8/edit?ts=5ab866ce]
 
<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Licencia Creative Commons Atribución-NoComercial 4.0 Internacional</a>.
