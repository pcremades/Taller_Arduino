# Construcción del Colorímetro

## Materiales
 - Arduino UNO o cualquier variante con 8 entradas/salidas digitales.
 - LED RGB infrarrojo [5050](http://tienda.ityt.com.ar/mercadolibre/5951-starter-kit-led-rgb-5050-arduino-itytarg.html?search_query=led+rgb&results=214) o equivalente
 - Sensor color [Tcs3200](http://tienda.ityt.com.ar/mercadolibre/1594-sensor-color-tcs230-tcs3200-arduino-itytarg.html?search_query=sensor+color&results=483) o equivalente 
 - [Carcasa](https://gitlab.com/pcremades/Taller_Arduino/tree/master/Colorimetro/Hardware/Carcasa) impresa en 3D.

## Armado

1. Inserte el sensor de color en la tapa correspondiente.

2. Pegue con adhesivo tipo silicona el LED RGB en la tapa correspondiente.

3. Inserte los cables que conectan el LED RGB al arduino a través del tabique de separación interno de la caja
del colorímetro.

4. Coloque las tapas del colorímetro en la posición que corresponde. La tapa con el LED RGB debe colocarse del
lado que va la cubeta con la muestra.

5. Conecte el sensor de color y el LED RGB siguiendo el esquema de la figura:

![Diagrama conexión colorímetro](https://gitlab.com/pcremades/Taller_Arduino/raw/master/Colorimetro/Img/Diagrama.png)

6. Cargue el [firmware](https://gitlab.com/pcremades/Taller_Arduino/tree/master/Colorimetro/Software/firmware.zip) en el Arduino.


<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Licencia Creative Commons Atribución-NoComercial 4.0 Internacional</a>.
