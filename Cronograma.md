# Cronograma

### Primer encuentro (4 hrs.):
Experiencias prácticas:
1. Cinemática. Caída libre. Determinación de la aceleración de la gravedad con
una fotocompuerta.
2. Colorimetría. Determinación de la concentración en una solución utilizando
el colorímetro.

### Segundo encuentro (4 hrs.):
Tecnología libre:
Conceptos básicos y la diferencia con tecnología propietaria.
Patentes y licencias.
Breve reseña histórica del software libre.
Tecnología libre en el ámbito científico y académico. Ejemplos.

### Tercer encuentro (4 hrs.):
Arduino:
Introducción a la plataforma Arduino. IDE de Arduino. Introducción a la programación.
Funciones setup() y loop().
Entradas y salidas digitales. Entradas analógicas. Comunicación con la PC.

### Cuarto encuentro (4 hrs.):
Fotocompuerta.
Medición de tiempo con fotocompuerta. Medición de velocidad con fotocompuerta.
Construcción de una fotocompuerta. Conceptos básicos de electrónica: tensión, corriente, ley de ohm. 
LED y fototransistor.

### Quinto encuentro (4 hrs.):
Herramientas de prototipado rápido y el movimiento DIY:
Herramientas de control numérico (CNC).
Impresoras 3D.

### Sexto encuentro (4 hrs.):
Introducción a Processing:
Introducción a programación orientada a objetos. IDE de Processing.
Definición de Clases. Definición de objetos.
Estructura básica de un programa en Processing. Funciones setup() y draw().
Captura de eventos de teclado y mouse.
Interfaz entre Arduino y Processing:
Comunicación por puerto serie. Captura y procesamiento de datos en Processing.
Adquisición, procesamiento y visualización de datos.
Aplicaciones prácticas con Processing: desarrollo de interfaces gráficas sencillas.

### Séptimo encuentro (4 hrs.):
Colorímetro.
Principio de funcionamiento. Ley de Beer-Lambert. Construcción del colorímetro.
LED RGB. Sensor de intensidad de luz. Curva de calibración del colorímetro.

### Octavo encuentro:
Evaluación: presentación oral de propuestas didácticas de laboratorio utilizando tecnología libre.
