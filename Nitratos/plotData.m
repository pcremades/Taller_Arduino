datos = dlmread("Nitratos-17-05-2019.csv", '\t', 1, 0)(1:end-3,:);

ppm = datos(:,1);
absUV = datos(:,2);
absCol = datos(:,7);

figure(1)
plot(ppm, absUV, ".ob", "markersize", 3)
hold on
plot(ppm, absCol, ".or", "markersize", 3)

[b, bint, r, rint, stats] = regress(absUV, ppm);
plot(ppm, ppm*b, "-g")

hold off